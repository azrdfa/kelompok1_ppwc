from django.contrib import admin
from .models import Model_Daftar, Model_Donasi, User_Data

admin.site.register(Model_Daftar)
admin.site.register(Model_Donasi)
admin.site.register(User_Data)