from django.urls import path
from django.conf.urls import url, include
from django.contrib.auth import views
from .views import *
from django.conf import settings

urlpatterns = [
	path('daftar', daftar, name = 'daftar'),
	path('logout', views.LogoutView.as_view(), {'next_page': settings.LOGOUT_REDIRECT_URL}, name = 'logout'),
	path('donasi', donasi, name = 'donasi'),
	path('terdonasi', savedatadonasi, name = 'terdonasi'),
    # url(r'^logout/$', views.logout, name='logout'),
    path('auth/', include('social_django.urls', namespace='social')),
	path('data_dict/', data_func, name="data_func")
]