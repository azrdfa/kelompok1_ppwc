from django import forms
from .models import Status
from django.forms import ModelForm

class Status_Message(forms.ModelForm):
	Kabar = forms.CharField(max_length= 300,label='',widget=forms.Textarea(attrs={ 'placeholder' : 'how was our website?', 'rows' : 5, 'cols' : 100}))
	Nama = "adi"
	class Meta:
		model = Status
		fields = ['Kabar']