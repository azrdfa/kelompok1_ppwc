from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import daftar, savedata, donasi, savedatadonasi
from .models import Model_Daftar, Model_Donasi, User_Data
from .forms import Form_Daftar, Form_Donasi
# Create your tests here.

class SinarPerakUnitTest(TestCase):
	
	def test_daftar_url_is_exist(self):
		response = Client().get('/crowd/daftar')
		self.assertEqual(response.status_code, 200)

	def test_daftar_using_daftar_func(self):
		found = resolve('/crowd/daftar')
		self.assertEqual(found.func, daftar)

	def test_daftar_using_formdaftar_template(self):
		response = Client().get('/crowd/daftar')
		self.assertTemplateUsed(response, 'formdaftar.html')

	def test_donasi_url_is_exist(self):
		response = Client().get('/crowd/donasi')
		self.assertEqual(response.status_code, 200)

	def test_donasi_using_donasi_func(self):
		found = resolve('/crowd/donasi')
		self.assertEqual(found.func, donasi)

	def test_donasi_using_formdonasi_template(self):
		response = Client().get('/crowd/donasi')
		self.assertTemplateUsed(response, 'formdonasi.html')
	
	# model test
	def test_model_can_create_new_user_data(self):
		# creating a new status
		new_activity = User_Data.objects.create(user_id=1, user_program="bantu donggala", user_money=1000)

		# retrieving all available status
		counting_all_available_status = User_Data.objects.all().count()
		self.assertEqual(counting_all_available_status, 1)



	# def test_can_save_a_POST_request(self):
	# 	response = self.client.post('/crowd/donasi/', data={'nama_program':'donggala', 'nama':'azhar', 'email':'a@gmail.com', 'jumlah':'1000'})
	# 	counting_all_available_status = User_Data.objects.all().count()
	# 	self.assertEqual(counting_all_available_status, 0)

	# 	self.assertEqual(response.status_code, 404)
	# 	# self.assertEqual(response['location'], '/crowd/terdonasi')