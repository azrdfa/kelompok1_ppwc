from django.urls import path
from .views import *
urlpatterns = [
    path('', index, name='index'),
    path('berita', viewnews, name= 'news'),
    path('berita/<int:id>/', news_detail, name='newsdetail'),

    path('program', viewprogram, name= 'program'),
    path('program/<int:id>/', dynamic, name= 'program'),
    path('comment',status, name= 'comment'),
    path('status/add_activity', status_add_view, name="status_add"),
    path('getuser/',getUser, name= 'getUser'),
]